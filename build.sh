find -type f -name Dockerfile | sed -r "s|/[^/]+$||" | sed "s/^..//" | while read path; do
	echo "Building $path"
	docker build --cache-from "$CI_REGISTRY_IMAGE/$path" --pull -t "$CI_REGISTRY_IMAGE/$path" $path
	docker push "$CI_REGISTRY_IMAGE/$path"
done
